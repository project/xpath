<?php
/**
 * @file
 * Contains xpath import form.
 */

/**
 * Form implementation of primary xpath_import multiform.
 */
function xpath_import_form($form, &$form_state) {
  // Check to see if anything has been stored.
  if ($form_state['rebuild']) {
    $form_state['input'] = array();
  }
  if (empty($form_state['storage'])) {
    // No step has been set so start with the first.
    $form_state['storage'] = array(
      'step' => 'xpath_import_first_form',
    );
  }

  // Return the current form
  $function = $form_state['storage']['step'];
  $form = $function($form, $form_state);
  return $form;
}

/**
 * Submit handler for the primary xpath_import multiform.
 *
 * @see xpath_import_form().
 */
function xpath_import_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  if (isset($values['back']) && $values['op'] == $values['back']) {
    // Moving back in form.
    $step = $form_state['storage']['step'];
    // Call current step submit handler if it exists to unset step form data.
    if (function_exists($step . '_submit')) {
      $function = $step . '_submit';
      $function($form, $form_state);
    }
    // Remove the last saved step so we use it next.
    $last_step = array_pop($form_state['storage']['steps']);
    $form_state['storage']['step'] = $last_step;
  }
  else {
    // Record step.
    $step = $form_state['storage']['step'];
    $form_state['storage']['steps'][] = $step;
    // Call step submit handler if it exists.
    if (function_exists($step . '_submit')) {
      $function = $step . '_submit';
      $function($form, $form_state);
    }
  }
  return;
}

/**
 * Menu callback form. First of three.
 */
function xpath_import_first_form($form, $form_state) {
  if (!empty($form_state['storage']))
    $values = $form_state['storage'];
  $options = _xpath_getcontenttypes();
  $form['xpath_import_content_type'] = array(
    '#type' => 'select',
    '#title' => t('Select Content Type'),
    '#required' => TRUE,
    '#options' => $options,
    '#ajax' => array(
      'callback' => '_xpath_getfields_ajax',
      'wrapper' => 'xpath-content-fields',
      'method' => 'replace',
      'effect' => 'fade',
    ),
    '#default_value' => isset($values['xpath_import_content_type']) ? $values['xpath_import_content_type'] : NULL,
  );
  $form['xpath_import_content_fields'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#prefix' => '<div id="xpath-content-fields">',
    '#description' => t('Map xpath of the data to the fields to be fetched.'),
    '#suffix' => '</div>',
  );

  // Default field setting.
  $display_fields = FALSE;
  if (isset($form_state['values']['xpath_import_content_type'])) {
    $display_fields = TRUE;
    $fields = field_info_instances("node", $form_state['values']['xpath_import_content_type']);
    if (isset($values['xpath_import_content_fields']))
      unset($values['xpath_import_content_fields']);
  }
  elseif (isset($values['xpath_import_content_type'])) {
    $display_fields = TRUE;
    $fields = field_info_instances("node", $values['xpath_import_content_type']);
  }


  if ($display_fields && isset($fields)) {
    $form['xpath_import_content_fields']['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Title'),
      '#description' => t('Enter xpath for this field'),
      '#default_value' => isset($values['xpath_import_content_fields']['title']) ? $values['xpath_import_content_fields']['title'] : NULL,
    );
    foreach ($fields as $field) {
      $form['xpath_import_content_fields'][$field['field_name']] = array(
        '#type' => 'textfield',
        '#title' => check_plain($field['label']),
        '#description' => t('Enter xpath for this field'),
        '#default_value' => isset($values['xpath_import_content_fields'][$field['field_name']]) ? $values['xpath_import_content_fields'][$field['field_name']] : NULL,
      );
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Next'),
  );
  return $form;
}

/**
 * Function returns array of available content types.
 */
function _xpath_getcontenttypes() {
  $types_oa = node_type_get_types();
  foreach ($types_oa as $o) {
    $types[$o->type] = $o->name;
  }
  return $types;
}

/**
 * Ajax callback.
 */
function _xpath_getfields_ajax($form, &$form_state) {
  return $form['xpath_import_content_fields'];
}

/**
 * Submit handler for the first form.
 *
 * @see multiform_first_form()
 */
function xpath_import_first_form_submit($form, &$form_state) {
  $values = $form_state['values'];

  // Save values to the form storage, add the next step function callback
  $form_state['rebuild'] = TRUE;  // This is very important to have!
  $form_state['storage']['xpath_import_content_type'] = $form_state['values']['xpath_import_content_type'];
  foreach ($form_state['values']['xpath_import_content_fields'] as $field => $value) {
      $form_state['storage']['xpath_import_content_fields'][$field] = $value;
  }
  $form_state['storage']['step'] = 'xpath_import_second_form';
}

/**
 * Form that is called second
 */
function xpath_import_second_form($form, &$form_state) {
  if (!empty($form_state['storage']))
    $values = $form_state['storage'];

  $form['xpath_import_test_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Enter Test URL for verification of Xpath\'s'),
    '#default_value' => isset($values['xpath_import_test_url']) ? $values['xpath_import_test_url'] : NULL,
  );

  $form['submit_url'] = array(
    '#type' => 'button',
    '#value' => t('Test'),
    '#name'  => 'test-button',
    '#ajax' => array(
        'callback' => '_xpath_import_test_url',
        'method'   => 'replace',
        'effect'   => 'fade',
        'wrapper' => 'xpath-import-test-data',
    ),
  );

  $form['xpath_import_test_data'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#prefix' => '<div id="xpath-import-test-data">',
    '#suffix' => '</div>',
  );

  // Default field setting.
  $display_fields = FALSE;
  if (isset($form_state['values']['xpath_import_test_url'])) {
    $display_fields = TRUE;
    $url = $form_state['values']['xpath_import_test_url'];
  }
  elseif (isset($values['xpath_import_test_url'])) {
    $display_fields = TRUE;
    $url = $values['xpath_import_test_url'];
  }


  if ($display_fields && isset($url)) {
    if ((isset($url)) && (empty($url) || !(url_is_external($url)))) {
      $form['xpath_import_test_data']['skip'] = array(
        '#markup' => '<div class="messages warning">' . t('Please Provide valid url') . '</div>',
      );
    }
    else {
      $default_values = _xpath_fetch_data($values['xpath_import_content_fields'], $url);
      foreach ($default_values as $field => $value) {
        $form['xpath_import_test_data'][$field] = array(
          '#type' => 'item',
          '#title' => check_plain($field),
          '#markup' => $value,
        );
      }
    }
  }

  $form['back'] = array(
    '#type' => 'submit',
    '#value' => t('Incorrect'),
    '#limit_validation_errors' => array(),
    '#submit' => array('xpath_import_second_form_submit'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Correct'),
  );
  return $form;
}

/**
 * Validator for test url.
 */
function _xpath_import_validate_test_url($form, $form_state) {
  if (!$form_state['values']['xpath_import_test_url'] || !url_is_external($form_state['values']['xpath_import_test_url'])) {
    form_set_error('xpath_import_test_url', t('Provide valid test url'));
  }
}

/**
 *
 */
function _xpath_import_test_url($form, $form_state) {
  return $form['xpath_import_test_data'];
}

/**
 * Submit handler for the second step.
 *
 * @see multiform_second_form_start()
 */
function xpath_import_second_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $form_state['rebuild'] = TRUE;
  if (isset($values['back']) && $values['op'] == $values['back']) {
    $input = $form_state['input'];
    // The user clicked the back button, save values that were input
    if (isset($input['xpath_import_test_url'])) {
      $form_state['storage']['xpath_import_test_url'] = $input['xpath_import_test_url'];
    }
    $form_state['storage']['step'] = 'xpath_import_first_form';
  }
  else {
    $form_state['storage']['xpath_import_test_url'] = $values['xpath_import_test_url'];
    $form_state['storage']['step'] = 'xpath_import_third_form';
  }
}

/**
 * Form that is called third
 */
function xpath_import_third_form(&$form, &$form_state) {
  $form['xpath_import_url_use'] = array(
    '#type' => 'select',
    '#title' => t('Import URL\'s'),
    '#options' => array(
      'direct_input' => t('Direct Input'),
      //'xpath_input' => t('Input using XPath'),
    ),
  );

  $form['xpath_import_urls'] = array(
    '#type' => 'textarea',
    '#title' => t('Input the URL\'s'),
    '#description' => t('Enter each url in separate line.'),
    '#states' => array(
      'visible' => array(
        ':input[name="xpath_import_url_use"]' => array('value' => 'direct_input'),
      ),
    ),
  );

  $form['back'] = array(
    '#type' => 'submit',
    '#value' => t('Back'),
    '#limit_validation_errors' => array(),
    '#submit' => array('xpath_import_third_form_submit'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  $form['#submit'][] = 'xpath_import_third_form_submit';

  return $form;
}

/**
 * Submit handler for the third step.
 *
 * @see multiform_third_form()
 */
function xpath_import_third_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $form_state['rebuild'] = TRUE;

  if (isset($values['back']) && $values['op'] == $values['back']) {
    // The user pushed the back button. Clear out the terms checkbox.
    $form_state['storage']['xpath_import_urls'] = NULL;
    $form_state['storage']['step'] = 'xpath_import_second_form';
  }
  else {
    $form_state['storage']['import_urls'] = htmlspecialchars($values['xpath_import_urls']);
    $form_state['storage']['step'] = 'xpath_import_form_complete';
  }
}

/**
 * The complete form (Thank you page) for parent form templates.
 */
function xpath_import_form_complete($form, &$form_state) {
  xpath_lr_build_batch($form_state['storage']);
}

/**
 * Building and processing nodes as patch.
 */
function xpath_lr_build_batch($data) {
  // Here we can add multiple operation using an array variable.
  $urls = explode(PHP_EOL, $data['import_urls']);
  foreach ($urls as $url) {
    $operations[] = array('_xpath_create_node', array($data['xpath_import_content_type'], $data['xpath_import_content_fields'], $url));
  }

  //Define your batch operation here
  $batch = array(
    'title' => t('Batch operation process'),
    'operations' => $operations,
    'finished' => 'xpath_lr_build_batch_finished',
    'init_message' => t('Initializing...'),
    'progress_message' => t('Opertation @current out of @total.'),
    'error_message' => t('Found some error here.'),
  );
  batch_set($batch);
  batch_process('admin/content/xpath');
}

function xpath_lr_build_batch_finished($success, $results, $operations) {
  if ($success) {
    // Here we could do something meaningful with the results.
    // We just display the number of data we processed...
    drupal_set_message(t('@count nodes  processed.', array('@count' => count($results))));
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    drupal_set_message(t('An error occurred while processing @operation with arguments : @args', array('@operation' => $error_operation[0], '@args' => print_r($error_operation[0], TRUE))));
  }
}
